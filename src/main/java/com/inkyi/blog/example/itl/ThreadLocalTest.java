package com.inkyi.blog.example.itl;

public class ThreadLocalTest {

    private static final ThreadLocal<String> TL_NAME = ThreadLocal.withInitial(String::new);

    private static final ThreadLocal<String> TL_SEX = ThreadLocal.withInitial(String::new);

    public static void main(String[] args) {

        TL_NAME.set("张三");
        TL_SEX.set("未知");
        System.out.println(TL_NAME.get());
        System.out.println(TL_SEX.get());

        Thread x = new Thread(()->{
            TL_NAME.set("李四");
            TL_SEX.set("女");
            System.out.println(TL_NAME.get());
            System.out.println(TL_SEX.get());
        });
        x.start();

        //main线程中 --> threadLocals
        /**
         * Key -> TL_NAME, value = ’张三‘
         * Kee -> TL_SEX, value = '男'
         */

        // 假如我们在创建一个线程X,还是使用TL_NAME和TL_SEX来存放值,那么在X线程的threadLocals变量中存放的应该如下
        /**
         * Key -> TL_NAME, value = ’李四‘
         * Kee -> TL_SEX, value = '女'
         */

    }

}
