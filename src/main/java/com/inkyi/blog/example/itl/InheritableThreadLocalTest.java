package com.inkyi.blog.example.itl;

public class InheritableThreadLocalTest {


    private static final InheritableThreadLocal<String> ITL = new InheritableThreadLocal<>();

    public static void main(String[] args) {
//        ITL.set("main存入的字符串");
        Thread t = new Thread(()->{
            ITL.get();
            System.out.println(ITL.get());
            ITL.set("Child存入的字符串");
        });
        t.start();
        System.out.println(ITL.get());
    }

}
