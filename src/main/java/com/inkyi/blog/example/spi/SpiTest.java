package com.inkyi.blog.example.spi;

import java.util.ServiceLoader;

public class SpiTest {
    public static void main(String[] args) {
        ServiceLoader<Waimai> load = ServiceLoader.load(Waimai.class);
        for (Waimai demo : load) {
            demo.songWaiMai();
        }
    }
}
