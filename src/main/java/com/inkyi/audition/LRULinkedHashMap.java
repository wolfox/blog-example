package com.inkyi.audition;

import java.util.LinkedHashMap;
import java.util.Map;

public class LRULinkedHashMap<K,V> extends LinkedHashMap<K,V> {

    private int initCapacity;

    public LRULinkedHashMap(int initialCapacity, int initCapacity) {
        this.initCapacity = initCapacity;
    }

    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
        return size() > initCapacity;
    }

}
