package com.inkyi.leetcode;

public class NodeUtils {



    public static ListNode getLinked(int size) {
        ListNode root = new ListNode(0);
        ListNode now = root;
        for (int i = 1; i < size; i++) {
            ListNode temp = new ListNode(i);
            now.next = temp;
            now = temp;
        }
        return root;
    }

    public static ListNode arrToLinked(int[] arr) {
        ListNode root = new ListNode(arr[0]);
        ListNode now = root;
        for (int i = 1; i < arr.length; i++) {
            ListNode temp = new ListNode(arr[i]);
            now.next = temp;
            now = temp;
        }
        return root;
    }

    public static void println(ListNode heard) {
        ListNode curr = heard;
        StringBuilder sb = new StringBuilder();
        while (curr != null){
            sb.append(curr.val).append(",");
            curr = curr.next;
        }
        System.out.println(sb);
    }

}
