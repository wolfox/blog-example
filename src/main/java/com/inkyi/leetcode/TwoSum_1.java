package com.inkyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum_1 {

    public static void main(String[] args) {
        int[] nums = new int[]{2,7,11,15};
        int target = 9;
        int len = nums.length;
        Map<Integer,Integer> table = new HashMap<>();

        for (int i = 0; i < len; i++) {
            int temp = target - nums[i];
            if(table.containsKey(temp)){
                int[] ints = {table.get(temp), i};
                System.out.println(Arrays.toString(ints));
            }else{
                table.put(nums[i], i);
            }

        }
    }



}
