package com.inkyi.leetcode;

/**
 * 反转链表
 */
public class ReverseLinkedList_206 {

    public static void main(String[] args) {
        ListNode root = NodeUtils.getLinked(4);
        ListNode reslut = reverseList(root);
        NodeUtils.println(reslut);
    }

    public static ListNode reverseList(ListNode head) {

        ListNode prve = null;
        ListNode curr = head;
        ListNode next = null;

        while (curr != null){
            //反指
            next = curr.next;
            curr.next = prve;
            //下一个
            prve = curr;
            curr = next;
        }

        return prve;
    }






}
